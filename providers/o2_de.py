import json


def invoice_function(data):
    invoices = []
    data = json.loads(str(data))
    for invoice in data['invoices']:
        invoice_id = invoice['billDocuments'][0]['billNumber']
        invoices.append(invoice_id)
    return invoices


invoice_data = dict({
"PROVIDER": "o2_de",
"LOGIN_URL":'https://login.o2online.de/auth/login?goto=https://www.o2online.de/mein-o2/',
"LOGIN_ACTION":"/sso/UI/Login",
"LOGIN_USER_FIELD":"IDToken1",
"LOGIN_PASSWORD_FIELD":"IDToken2",
"INVOICE_LIST":"https://www.o2online.de/vt-billing/api/invoiceinfo",
"INVOICE_FUNCTION": invoice_function,
"INVOICE_PDF_FILE": 'https://www.o2online.de/vt-billing/api/billdocument?billNumber={0}&documentType=BILL'
})



