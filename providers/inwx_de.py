from fetchers import Fetcher
from .inwx.inwx import domrobot, prettyprint, getOTP
from pathlib import Path


class Inwx(Fetcher):
    NAME = "inwx_de"
    def __init__(self, username, password, shared_secret, output_directory='output'):
        super().__init__(username, password, output_directory='output')
        print("Fetching invoices for", self.NAME, "...")
        self.shared_secret = shared_secret
        api_url = "https://api.domrobot.com/xmlrpc/"
        self.inwx_conn = domrobot(api_url, False)

    def login(self):

        loginRet = self.inwx_conn.account.login({'lang': 'en', 'user': self.username, 'pass': self.password})

        if 'resData' in loginRet:
            loginRet = loginRet['resData']

        if 'tfa' in loginRet and loginRet['tfa'] == 'GOOGLE-AUTH':
            loginRet = self.inwx_conn.account.unlock({'tan': getOTP(self.shared_secret)})

    def get_invoices(self):
        invoices = []
        data = self.inwx_conn.accounting.listInvoices([])
        for item in data['resData']['invoice']:
            invoices.append(item["invoiceId"])

        return invoices

    def get_invoice(self, invoice_id):
        return self.inwx_conn.accounting.getInvoice({'invoiceId': invoice_id})['resData']['pdf'].data

    def fetch_invoices(self):
        self.login()
        for invoice in self.get_invoices():
            filename = 'bill_{1}_{0}.pdf'.format(self.sane(invoice), self.NAME)
            invoice_file = Path.cwd().joinpath(self.output_directory, filename)
            if not invoice_file.is_file():
                print("Found invoice", invoice)
                fp = open(invoice_file, "wb")
                fp.write(self.get_invoice(invoice))
                fp.close()
            else:
                print("Skipping invoice", invoice, "as it was downloaded before...")

