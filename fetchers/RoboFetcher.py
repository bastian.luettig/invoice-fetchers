from robobrowser import RoboBrowser
from pathlib import Path
from . import Fetcher


class RoboFetcher(Fetcher):

    def __init__(self, invoice_data, username, password, output_directory='output'):
        super().__init__(username, password, output_directory)
        self.browser = RoboBrowser(parser="html.parser")
        self.data = invoice_data
        print("Fetching invoices for", self.get_data("PROVIDER"), "...")

    def login(self):
        self.browser.open(self.get_data("LOGIN_URL"))
        form = self.browser.get_form(action=self.get_data("LOGIN_ACTION"))
        form[self.get_data("LOGIN_USER_FIELD")].value = self.username 
        form[self.get_data("LOGIN_PASSWORD_FIELD")].value = self.password
        self.browser.submit_form(form)

    def get_data(self, key):
        return self.data[key]


    def get_invoices(self):

        self.browser.open(self.get_data("INVOICE_LIST"))
        return self.get_data("INVOICE_FUNCTION")(self.browser.parsed)

    def fetch_invoices(self):
        self.login()
        for invoice in self.get_invoices():
            filename = 'bill_{1}_{0}.pdf'.format(self.sane(invoice), self.get_data("PROVIDER"))
            invoice_file = Path.cwd().joinpath(self.output_directory, filename)
            if not invoice_file.is_file():
                print("Found invoice", invoice)
                invoice_url=self.get_data("INVOICE_PDF_FILE").format(invoice)
                self.browser.open(invoice_url)

                fp = open(invoice_file, "wb")
                fp.write(self.browser.response.content)
                fp.close()
            else:
                print("Skipping invoice", invoice, "as it was downloaded before...")

