class Fetcher():
    keepcharacters = ('_')

    def __init__(self, username, password, output_directory='output'):
        self.username = username
        self.password = password
        self.output_directory = output_directory

    def sane(self, string):

        return "".join(c for c in string if c.isalnum() or c in self.keepcharacters).rstrip()

