def invoice_function(data):
    invoices = []
    mydivs = data.findAll("p", {"class": "pdf"})

    for div in mydivs:
        try:
            url = div.find("a", string="Rechnung")["href"]
            invoice_id = url.replace("/mytariff/invoice/showPDF/", "")
            invoices.append(invoice_id)
        except TypeError:
            print("this is a einzelverbindungsnachweis")
    return invoices


invoice_data = dict({
"PROVIDER": "winsim_de",
"LOGIN_URL":'https://service.winsim.de/',
"LOGIN_ACTION":"/public/login_check",
"LOGIN_USER_FIELD":"UserLoginType[alias]",
"LOGIN_PASSWORD_FIELD":"UserLoginType[password]",
"INVOICE_LIST":"https://service.winsim.de/mytariff/invoice/showAll",
"INVOICE_FUNCTION": invoice_function,
"INVOICE_PDF_FILE": 'https://service.winsim.de/mytariff/invoice/showPDF/{0}'
})



