# invoice-fetchers

Invoice fetchers for various providers

# Installation

I recommend using a virtual environment.

```shell
virtualenv-3 venv
source venv/bin/activate
pip3 install -r requirements.txt
```

# Usage
Create copy of conf.py, add one line per provider and user, add username and password for provider.

```python
import fetcher, providers.o2_de
from fetchers.RoboFetcher import RoboFetcher

RoboFetcher(providers.o2_de.invoice_data, "user", "password").fetch_invoices()
```


# Acknowledgements

inwx library taken from <https://github.com/inwx/python2.7-client>
